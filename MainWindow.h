#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include"IOutput.h"
#include "NewPopulationDialog.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
        , IOutput
{
    Q_OBJECT

public:

    virtual void Print(const std::string& msg);

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int width = 0;
    int height = 0;
    int totalCells = 0;

private slots:
    void on_actionNew_clicked();
    void on_enterButton_clicked();
    void on_pushButton_clicked();

private:
    NewPopulationDialog iw;
    std::string data_lab;
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
