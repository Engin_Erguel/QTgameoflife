#pragma once
#include<vector>
#include<string>
#include"Cell.h"
#include"InputOutput.h"

class Population
{
public:
	IOutput* output = 0;
	
	Population(IOutput* output, int width, int height);
	Population(IOutput* output);
	~Population();	
	std::vector<Cell> matrix;
	int widthX = 0;
	int heightY = 0;
	int defaultWidthX = 10;
	int defaultHeigthY = 10;
	int totalCells = 0;
	int livingCellsInFile = 0;
	int cellXFromFile = 0;
	int cellYFromFile = 0;

//	int GetMatrixWidth();
//	int GetMatrixHeight();
//	int GetInputNumber(const std::string& inputPrompt, const int minValue, const int maxValue, const std::string& inputPromptWhile);

	int CalculateTotalCells();
	void ResizeMatrix(int width, int height);
    //void DefineStartgeneration();
	void SetRandomCoordinates();
	void SetOwnCoordinates();
	Cell& GetCell(int x, int y);
	const Cell& GetCell(int x, int y) const;
	int NormalizeValue(int n, int max) const;
	int CountDeadCells();

	void SetPattern();
	void SetPatternBlinker();
	void SetPatternTrafficlight();
	void SetPatternBlock();
	void SetPatternGlider();
	void SetPatternBite();

private:
	Population();
	int inputTotalCellnumber = 0;
	int GetCellIndex(int x, int y) const;	
};
