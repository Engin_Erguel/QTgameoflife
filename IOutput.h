#pragma once
#include <QtGui>

struct IOutput
{
    virtual void Print(const std::string &msg) = 0;
    //virtual void IntInput(int& inp) = 0;
    //virtual void CharInput(char& inp) = 0;
};
