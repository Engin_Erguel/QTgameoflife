#pragma once
#include<string>
#include"IOutput.h"

int GetInputChar(const std::string& inputPrompt, const std::string& inputPromptWhile, int simulationStatus, IOutput* output);
void ShowCursor(bool showFlag);
void SetWindowLayout();