#include "MainWindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QDialog>
#include <QMessageBox>
#include "NewPopulationDialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::on_enterButton_clicked);                  //SIGNAL(clicked) SLOT(on_enterButton_clicked())
    connect(ui->actionNew, &QAction::triggered, this, &MainWindow::on_actionNew_clicked);
    //  connect(QRadioButton::radioButton, &QPushButton::clicked, this, &mainwindow::on_actionNewButton_clicked);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_enterButton_clicked()
{
    for (int y = 0; y < 10; ++y)
    {
        Print("\n");
        for (int x = 0; x < 10; ++x)
        {
            Print("1");
        }
    }
}

void MainWindow::Print(const std::string &input)
{
    data_lab += input;
    QString qstr = QString::fromStdString(data_lab);
    ui->textEdit->setAlignment(Qt::AlignCenter);
    ui->textEdit->setText(qstr);
}

void MainWindow::on_actionNew_clicked()
{
    NewPopulationDialog dialog(this);
    dialog.SetMsg("Input your values");
    dialog.SetLabelText();
    int result = dialog.exec();
    if (result != QDialog::DialogCode::Accepted)
    {
        return;
    }

    width = dialog.GetWidth();
    height = dialog.GetHeight();


    //dialog.exec();

    totalCells = dialog.GetTotalCells(width, height);


    QString t = QString::number(width);
    QMessageBox msgBox(this);
    msgBox.setText(t);
    msgBox.exec();
}

void MainWindow::on_pushButton_clicked()
{

}
