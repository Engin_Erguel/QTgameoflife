//#include "Population.h"
//#include<cassert>
//#include <sstream>
//#include<iostream>

//Population::Population(IOutput* output, int width, int height)
//{
//    this->output = output;
//    ResizeMatrix(width, height);
//}

//Population::Population(IOutput* output)
//{
//    this->output = output;
//    ResizeMatrix(defaultWidthX, defaultHeigthY);
//}

//Population::~Population()
//{
//}

////int Population::GetMatrixWidth()
////{
////    //std::string promptWidth("\nEnter matrix - width (max. 30): \n");
////    std::string promptWidthWhile("\nThe entered value is less than 0 or greater than 30. Enter a new value: \n");
////    return GetInputNumber(promptWidth, 0, 30, promptWidthWhile);
////}

////int Population::GetMatrixHeight()
////{
////    //std::string promptHeight("\nEnter matrix - height (max. 30): \n");
////    std::string promptHeightWhile("\nThe entered value is less than 0 or greater than 30. Enter a new value: \n");
////    return GetInputNumber(promptHeight, 0, 30, promptHeightWhile);
////}

////int Population::GetInputNumber(const int minValue, const int maxValue,
////    const std::string& inputPromptWhile)
////{
////    int inputNumber = 0;

////    //output->Print(inputPrompt);
////    output->IntInput(inputNumber);

////    while (inputNumber < minValue || inputNumber > maxValue)
////    {
////        std::cin.clear();
////        std::cin.ignore();
////        output->Print(inputPromptWhile);
////        output->IntInput(inputNumber);
////    }
////    return inputNumber;
////}

//int Population::CalculateTotalCells()
//{
//    return this->widthX * this->heightY;
//}

//void Population::ResizeMatrix(int width, int height)
//{
//    this->heightY = height;
//    this->widthX = width;
//    this->totalCells = CalculateTotalCells();

//    matrix.resize(this->totalCells);
//}

////void Population::DefineStartgeneration()
////{
////    char randomCoordinates = ' ';
////    std::ostringstream defineStartgenerationText;
////    defineStartgenerationText << "\nEnter number of total cells in start generation (0 - " << totalCells << "):\n";
////    const std::string defineStartgenerationTextWhile = "\nEntered value is greater than total number of cells or less than 0. Enter a new value: \n";
////    inputTotalCellnumber = GetInputNumber(defineStartgenerationText.str(), 0, totalCells, defineStartgenerationTextWhile);

////    output->Print("\nDo you want a random population? <y> <n>\n");
////    output->CharInput(randomCoordinates);

////    while (randomCoordinates != 'y' && randomCoordinates != 'n')
////    {
////        output->Print("\nWrong Input!\nDo you want a random population? <y> <n>\n");
////        output->CharInput(randomCoordinates);
////    }

////    switch (randomCoordinates)
////    {
////    case 'y':
////        SetRandomCoordinates();
////        break;

////    case 'n':
////        SetOwnCoordinates();
////        break;

////    default:
////        break;
////    }
////}

//void Population::SetRandomCoordinates()
//{
//    for (int i = 1; i <= inputTotalCellnumber; ++i)
//    {
//        int x = rand() % this->totalCells;
//        int y = rand() % this->totalCells;
//        Cell& cell = GetCell(x, y);
//        if (cell.getState() == cell.alive)
//        {
//            ++inputTotalCellnumber;
//        }
//        cell.setState(cell.alive);
//    }
//}

//void Population::SetOwnCoordinates()
//{
//   // system("cls");
//    //ShowCursor(true);
//    for (int i = 1; i <= inputTotalCellnumber; ++i)
//    {
//        int x = 0;
//        int y = 0;
//        char placeholder = ' ';

//        std::ostringstream cellValues;
//        cellValues << i << ". Cell x - value (from 0 to " << widthX - 1
//            << "), y - value (from 0 to " << heightY - 1 << ")" << std::endl;
//        output->Print(cellValues.str());
//        output->Print("Enter coordinates as: x,y:		");
//        std::cin >> x >> placeholder >> y;
//        output->Print("\n");
//        Cell& cell = GetCell(x, y);
//        cell.setState(1);
//    }
//   // ShowCursor(false);
//}

//void Population::SetPattern()
//{
//    int selectPattern = 0;
//    const int blinker = 1;
//    const int trafficLight = 2;
//    const int block = 3;
//    const int glider = 4;
//    const int bite = 5;
//    int totalSelection = 5;

//    const std::string patternSelectionText = "\nChoose pattern: \n<1> Blinker\n<2> Traffic light\n<3> Block\n<4> Glider\n<5> Bite\n";
//    const std::string patternSelectionTextWhile = "\nEnter new value: ";

//    //selectPattern = GetInputNumber(patternSelectionText, 1, totalSelection, patternSelectionTextWhile);

//    switch (selectPattern)
//    {
//    case blinker:
//        SetPatternBlinker();
//        break;
//    case trafficLight:
//        SetPatternTrafficlight();
//        break;
//    case block:
//        SetPatternBlock();
//        break;
//    case glider:
//        SetPatternGlider();
//        break;
//    case bite:
//        SetPatternBite();
//    default:
//        break;
//    }
//}

//void Population::SetPatternBlinker()
//{
//    for (int x = 3; x < 6; ++x)
//    {
//        Cell& cell = GetCell(x, 4);
//        cell.setState(1);
//    }
//}

//void Population::SetPatternTrafficlight()
//{
//    for (int y = 5; y > 2; --y)
//    {
//        GetCell(2, y).setState(1);
//        GetCell(8, y).setState(1);
//    }

//    for (int x = 4; x < 7; ++x)
//    {
//        GetCell(x, 1).setState(1);
//        GetCell(x, 7).setState(1);
//    }
//}

//void Population::SetPatternBlock()
//{
//    for (int y = 5; y > 3; --y)
//    {
//        GetCell(5, y).setState(1);
//        GetCell(6, y).setState(1);
//    }
//}

//void Population::SetPatternGlider()
//{
//    for (int x = 4; x < 7; ++x)
//    {
//        GetCell(x, 7).setState(1);
//    }
//    GetCell(6, 6).setState(1);
//    GetCell(5, 5).setState(1);
//}

//void Population::SetPatternBite()
//{
//    for (int x = 0; x < 10; ++x)
//    {
//        GetCell(x, 5).setState(1);
//    }
//}

//Cell& Population::GetCell(int x, int y)
//{
//    x = NormalizeValue(x, widthX);
//    y = NormalizeValue(y, heightY);
	
//    return matrix[GetCellIndex(x,y)];
//}

//const Cell& Population::GetCell(int x, int y) const
//{
//    x = NormalizeValue(x, widthX);
//    y = NormalizeValue(y, heightY);

//    return matrix[GetCellIndex(x, y)];
//}

//int Population::NormalizeValue(int n, int max) const
//{
//    assert(max != 0);

//    if (n < 0)
//    {
//        n = (max + (n % max)) % max;
//    }
//    else if (n >= max)
//    {
//        n = n % max;
//    }

//    return n;
//}

//int Population::GetCellIndex(int x, int y) const
//{
//    return x + y * this->widthX;
//}

//int Population::CountDeadCells()
//{
//    int deadCells = 0;

//    for (int y = 0; y < heightY; ++y)
//    {
//        for (int x = 0; x < widthX; ++x)
//        {
//            Cell& cell = GetCell(x, y);
//            int state = cell.getState();
//            if (state == cell.dead)
//            {
//                ++deadCells;
//            }
//        }
//    }
//    return deadCells;
//}
