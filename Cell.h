#ifndef CELL_H
#define CELL_H

class Cell
{
public:
	Cell();
	~Cell();

	int dead = 0;
	int alive = 1;

	int getState() const
	{
		return state;
	}

	void setState(int n)
	{
		state = n;
	}

private:
	int state = 0;
};

#endif