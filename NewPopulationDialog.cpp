#include "NewPopulationDialog.h"
#include "ui_NewPopulationDialog.h"
#include <QInputDialog>
#include <QDir>
#include <QDialog>
#include <QDialogButtonBox>
#include <QtGui>
#include <QDialog>
#include <QMessageBox>
#include <QMainWindow>
#include <QAction>


NewPopulationDialog::NewPopulationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewPopulationDialog)
{
    ui->setupUi(this);
    //connect(ui->comboBox_width, &QComboBox::currentIndexChanged, this, &NewPopulationDialog::on_comboBoxWidth_clicked);
    //connect(ui->comboBox_width, QOverload<int>::of(&QComboBox::currentIndexChanged),[=](int index){ /* ... */ });
    connect(ui->comboBox_width, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboBoxWidth_clicked()));
    connect(ui->comboBox_height, SIGNAL(currentIndexChanged(int)), this, SLOT(on_comboBoxHeight_clicked()));
}


NewPopulationDialog::~NewPopulationDialog()
{
    delete ui;
}

 void NewPopulationDialog::on_comboBoxWidth_clicked()
 {
     width = ui->comboBox_width->currentText().toInt();
     SetLabelText();
 }

 void NewPopulationDialog::on_comboBoxHeight_clicked()
 {
     width = ui->comboBox_height->currentText().toInt();
     SetLabelText();
 }

int NewPopulationDialog::GetWidth()
{
   return width;
}

int NewPopulationDialog::GetHeight()
{
    return ui->comboBox_height->currentText().toInt();
}

int NewPopulationDialog::GetTotalCells(int width, int height)
{
    ui->spinBox_totalCells->setMaximum(width*height);
    return ui->spinBox_totalCells->value();
}

void NewPopulationDialog::SetMsg(const QString& msg)
{
    this->setWindowTitle(msg);
}

void NewPopulationDialog::SetLabelText()
{
    ui->MatrixSize->setText("Size:"
                         + QString::number(ui->comboBox_width->currentText().toInt() * ui->comboBox_height->currentText().toInt()));
   // ui->spinBox_totalCells->setEnabled(true);
   // ui->checkBox_randomPop->setEnabled(true);
}
