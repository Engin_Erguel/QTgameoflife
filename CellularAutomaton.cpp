//#include "CellularAutomaton.h"
//#include<iomanip>
//#include<windows.h>
//#include<iostream>
//#include<fstream>
//#include <sstream>
//#include<conio.h>


//CellularAutomaton::CellularAutomaton(IOutput* output)
//    : pop(output)
//{
//    this->output = output;
//}

//CellularAutomaton::~CellularAutomaton()
//{
//}

//namespace inputcode
//{
//    const int defineStartgeneration = 1;
//    const int showSpecifiedPattern = 2;
//    const int readFromFile = 3;
//    const int exitSimulation = 0;
//}

//void CellularAutomaton::DoMainLoop()
//{
//    do
//    {
//        ChooseSimulationstyle();
//        //SelectSpecifiedStartgeneration();
//        waitingTimeInMilliseconds = 800;
//        DoSimulationLoop();
//    } while (simulationStatus == inputPhase);
//}

//void CellularAutomaton::ChooseSimulationstyle()
//{
//    int waitingTime = 600;
//    system("cls");
//    output->Print("Define your own first generation <1>\nSpecified pattern <2>\nRead pattern from file <3>\nExit <0>\n");

//    std::ostringstream ossBye;
//    ossBye << "\n" << std::setw(21) << "Bye!";

//    //CheckIfInputValid();

//    switch (inputSimulationstyle)
//    {
//    case inputcode::defineStartgeneration:
//        pop.widthX = mw.width;
//        pop.heightY = mw.height;
//        pop.totalCells = mw.totalCells;
//        this->pop.ResizeMatrix(pop.widthX, pop.heightY);
//        break;
//    case inputcode::showSpecifiedPattern:
//        pop.widthX = pop.defaultWidthX;
//        pop.heightY = pop.defaultHeigthY;
//        pop.totalCells = pop.CalculateTotalCells();
//        this->pop.ResizeMatrix(pop.widthX, pop.heightY);
//        break;
//    case inputcode::readFromFile:
//        pop.widthX = ReadPatternFromFile();
//        pop.heightY = pop.widthX;
//        pop.totalCells = pop.CalculateTotalCells();
//        this->pop.ResizeMatrix(pop.widthX, pop.heightY);
//        break;
//    case 0:
//        output->Print(ossBye.str());
//        Sleep(waitingTime);
//        exit(0);
//    }
//}

//void CellularAutomaton::CheckIfInputValid()
//{
//    bool inputOK = false;
//    std::string temp = "";

//    while (!inputOK || !IsInputValid())
//    {
//        try
//        {
//            std::cin.seekg(0, std::ios::beg);
//            getline(std::cin, temp);
//            inputSimulationstyle = stoi(temp);
//        }
//        catch (std::invalid_argument const& e)
//        {
//            std::ostringstream exceptionText;
//            exceptionText << '\n' << e.what() << ". Try again: ";
//            output ->Print(exceptionText.str());
//            continue;
//        }

//        if (IsInputValid())
//        {
//            inputOK = true;
//        }
//        else
//        {
//            output->Print("Entered value is invalid. Try again: ");
//        }
//    }
//}

//bool CellularAutomaton::IsInputValid()
//{
//    return inputSimulationstyle == inputcode::defineStartgeneration
//        || inputSimulationstyle == inputcode::showSpecifiedPattern
//        || inputSimulationstyle == inputcode::readFromFile
//        || inputSimulationstyle == inputcode::exitSimulation;
//}

////void CellularAutomaton::SelectSpecifiedStartgeneration()
////{
////    if (inputSimulationstyle == inputcode::defineStartgeneration)
////    {
////        pop.DefineStartgeneration();
////    }
////    else if (inputSimulationstyle == inputcode::showSpecifiedPattern)
////    {
////        pop.SetPattern();
////    }
////    else if (inputSimulationstyle == inputcode::readFromFile)
////    {
////        SetCoordinatesFromFile();
////    }

////    simulationStatus = drawPhase;
////}

//void CellularAutomaton::DoSimulationLoop()
//{
//    while (simulationStatus == drawPhase)
//    {
//        DrawWindow();
//        ProcessInput();
//        Sleep(waitingTimeInMilliseconds);
//        CheckIfStepwiseSimulation();
//        CalculateNextGeneration();

//        if (simulationStatus == inputPhase)
//        {
//            break;
//        }
//        else
//        {
//            EndSimulation();
//        }
//    }
//}

//void CellularAutomaton::DrawWindow()
//{
//    system("cls");
//    GiveMeTimeToPause();
//    DrawMatrix();
//    PrintOptionText();
//}

//void CellularAutomaton::GiveMeTimeToPause()
//{
//    if (generationCounter == 1)
//    {
//        Sleep(waitingTimeInMilliseconds);
//    }
//}

//void CellularAutomaton::DrawMatrix()
//{
//    for (int y = 0; y < pop.heightY; ++y)
//    {
//        std::cout << std::setw(10) << std::endl;

//        for (int x = 0; x < pop.widthX; ++x)
//        {
//            FormatOutput(x, y);
//        }
//    }
//}

//void CellularAutomaton::FormatOutput(int x, int y)
//{
//    const Cell& cell = pop.GetCell(x, y);
//    int state = cell.getState();

//    if (state == cell.dead)
//    {
//        std::ostringstream charDead;
//        charDead << char(250) << ' ';
//        output->Print(charDead.str());
//    }
//    else
//    {
//        std::ostringstream charAlive;
//        charAlive << char(254) << ' ';
//        output->Print(charAlive.str());
//    }
//}

//void CellularAutomaton::PrintOptionText()
//{
//    std::ostringstream ossOptionPause;
//    ossOptionPause << "\n\n" << std::setw(27) << "Pause <Space>" << std::endl;
	
//    std::ostringstream ossOptionExit;
//    ossOptionPause << std::setw(27) << "Exit <Return to main menu>" << std::endl;

//    std::ostringstream ossOptionSpeed;
//    ossOptionPause << std::setw(27) << "Faster <+> Slower <->" << std::endl;

//    std::ostringstream ossOptionStepwise;
//    ossOptionPause << std::setw(27) << "Stepwise simulation <s>" << std::endl;

//    std::ostringstream ossOptionContinuous;
//    ossOptionPause << std::setw(27) << "Continuous simulation <d>" << std::endl;

//    std::ostringstream ossOptionGeneration;
//    ossOptionPause << "\n\n" << std::setw(24) << "Generation: " << generationCounter << '\n' << std::endl;

//    output->Print(ossOptionPause.str());
//    output->Print(ossOptionExit.str());
//    output->Print(ossOptionSpeed.str());
//    output->Print(ossOptionStepwise.str());
//    output->Print(ossOptionContinuous.str());
//    output->Print(ossOptionGeneration.str());
//}

//void CellularAutomaton::ProcessInput()
//{
//    if (_kbhit())
//    {
//        const char space = ' ';
//        const int escape = 27;
//        const int enter = 13;
//        const int waitingtime = 650;

//        std::ostringstream ossPause;
//        ossPause << "\n" << std::setw(22) << "PAUSE";
//        std::ostringstream ossReturn;
//        ossReturn << "\n" << std::setw(22) << "RETURN";

//        char keyHit = _getch();
//        switch (keyHit)
//        {
//        case space:
//            output->Print(ossPause.str());
//           // system("pause>nul");
//            break;
//        case escape:
//            output->Print(ossReturn.str());
//            Sleep(waitingtime);
//            generationCounter = 0;
//            simulationStatus = inputPhase;
//            fill(pop.matrix.begin(), pop.matrix.end(), Cell());
//            break;
//        case '+':
//            if (waitingTimeInMilliseconds != 8)
//            {
//                waitingTimeInMilliseconds -= 99;
//            }
//            break;
//        case '-':
//            waitingTimeInMilliseconds += 99;
//            break;
//        case 's':
//            stepwiseSimulation = true;
//            break;
//        }
//    }
//}

//void CellularAutomaton::CheckIfStepwiseSimulation()
//{
//    if (stepwiseSimulation == true)
//    {
//        system("cls");
//        DrawMatrix();
//        PrintOptionText();
//        char keyHit = _getch();
//        switch (keyHit)
//        {
//        case 'd':
//            stepwiseSimulation = false;
//            break;
//        }
//    }
//}

//void CellularAutomaton::CalculateNextGeneration()
//{
//    Population temp(output, this->pop.widthX, this->pop.heightY);
//    CalculateNewCellStates(this->pop, temp);
//    this->pop = temp;
//    ++generationCounter;
//}

//void CellularAutomaton::CalculateNewCellStates(const Population& source, Population& destination)
//{
//    for (int y = 0; y < destination.heightY; ++y)
//    {
//        for (int x = 0; x < destination.widthX; ++x)
//        {
//            CalculateNewCellState(x, y,source, destination);
//        }
//    }
//}

//void CellularAutomaton::CalculateNewCellState(int x, int y, const Population& source, Population& destination)
//{
//    int newState = DetermineNewCellState(x, y, source);
//    SetNewCellState(x, y, destination, newState);
//}

//int CellularAutomaton::DetermineNewCellState(int x, int y, const Population& source)
//{
//    const int neighborOffsetX[8] = { -1, -1, -1, 0, 1, 1,  1,  0 };
//    const int neighborOffsetY[8] = { -1,  0,  1, 1, 1, 0, -1, -1 };
//    int livingNeighborCells = 0;

//    for (int n = 0; n < 8; ++n)
//    {
//        const Cell& cell = source.GetCell(x + neighborOffsetX[n], y + neighborOffsetY[n]);
//        int state = cell.getState();
//        if (state == cell.alive)
//        {
//            ++livingNeighborCells;
//        }
//    }

//    int newState = 0;
//    const Cell& cell = source.GetCell(x, y);
//    int myState = cell.getState();
	
//    if (myState != cell.dead)
//    {
//        if (livingNeighborCells >= deadLonelinessLimit && livingNeighborCells <= surviveLimit)
//        {
//            newState = cell.alive;
//        }
//        else
//        {
//            newState = cell.dead;
//        }
//    }
//    else
//    {
//        if (livingNeighborCells == surviveLimit)
//        {
//            newState = cell.alive;
//        }
//        else
//        {
//            newState = cell.dead;
//        }
//    }
//    return newState;
//}

//void CellularAutomaton::SetNewCellState(int x, int y, Population& destination, int newState)
//{
//        Cell& cell = destination.GetCell(x, y);
//        cell.setState(newState);
//}

//void CellularAutomaton::EndSimulation()
//{
//    int deadCells = pop.CountDeadCells();

//    if (deadCells == pop.totalCells)
//    {
//        std::ostringstream endSimulationText;
//        endSimulationText << "\n" << std::setw(25) << "Start again?" << "\n" << std::setw(26) << "yes <y> no <n>" << std::endl;
//        const std::string endSimulationTextWhile = "\nEnter <y> or <n>: ";
		
//        DrawWindow();
//        std::ostringstream ossDeadCells;
//        ossDeadCells << std::setw(29) << "All cells are dead." << std::endl;
//        output->Print(ossDeadCells.str());
//        Sleep(waitingTimeInMilliseconds);
//        simulationStatus = GetInputChar(endSimulationText.str(), endSimulationTextWhile, simulationStatus, output);
//        generationCounter = 1;
//    }
//}

//int CellularAutomaton::ReadPatternFromFile()
//{
//    std::ifstream file("test.txt");
//    int lineCounter = 1;
//    std::string line;
//    int longestLine = 0;
//    int errorCounter = 0;
	
//    system("cls");
	
//    for (lineCounter; getline(file, line); ++lineCounter)
//    {
//        longestLine = CheckIfPatternValid(line, lineCounter, longestLine, errorCounter);
//    }

//    EndIfError(errorCounter);

//    return longestLine;
//}

//int CellularAutomaton::CheckIfPatternValid(const std::string& line, int lineCounter, int longestLine, int& errorCounter)
//{
//    int charCounter = 0;

//    for (charCounter; charCounter < line.length(); ++charCounter)
//    {
//        if (!(line[charCounter] == '0' || line[charCounter] == '1'))
//        {
//            std::ostringstream ossWrongNumber;
//            ossWrongNumber << "WARNING: Only 0 and 1 are allowed! Invalid character in line: "
//                << lineCounter << " Column: " << charCounter + 1;

//            output->Print(ossWrongNumber.str());
//            ++errorCounter;
//        }
//        else if (line[charCounter] == ' ')
//        {
//            std::ostringstream ossSpaceError;
//            ossSpaceError << "WARNING: No spaces allowed! Invalid character in line: "
//                << lineCounter << " Column: " << charCounter + 1;

//            output->Print(ossSpaceError.str());
//            ++errorCounter;
//        }
//        else if (line[charCounter] == '1')
//        {
//            ++pop.livingCellsInFile;
//        }

//        if (longestLine < line.size())
//        {
//            longestLine = line.size();
//        }
//    }

//    return longestLine;
//}

//void CellularAutomaton::EndIfError(int errorCounter)
//{
//    if (errorCounter != 0)
//    {
//        output->Print("\n\n\nThe game will be closed. Please correct your errors.\n");
//        system("pause>NUL");
//        exit(0);
//    }
//}

//void CellularAutomaton::SetCoordinatesFromFile()
//{
//    std::ifstream file("test.txt");
//    std::string line;

//    for (int lineCounter = 0; getline(file, line); ++lineCounter)
//    {
//        for (int charCounter = 0; charCounter < line.length(); ++charCounter)
//        {
//            if (line[charCounter] == '1')
//            {
//                pop.GetCell(charCounter, lineCounter).setState(1);
//            }
//        }
//    }
//}
