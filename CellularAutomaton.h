#pragma once
#include"Population.h"
#include"IOutput.h"
#include"InputOutput.h"
#include<string>
#include"MainWindow.h"

class CellularAutomaton
{
public:
	Population pop;
	IOutput* output = 0;
    MainWindow mw;
	
	CellularAutomaton(IOutput* output);
	~CellularAutomaton();

	void DoMainLoop();
	void ChooseSimulationstyle();
	void CheckIfInputValid();
	bool IsInputValid();
    //void SelectSpecifiedStartgeneration();
	
	void DoSimulationLoop();
	
	void DrawWindow();
	void GiveMeTimeToPause();
	void DrawMatrix();
	void FormatOutput(int x, int y);
	void PrintOptionText();
	
	void ProcessInput();
	void CheckIfStepwiseSimulation();
	void CalculateNextGeneration();
	void CalculateNewCellStates(const Population& source, Population& destination);
	void CalculateNewCellState(int x, int y, const Population& source, Population& destination);
	int DetermineNewCellState(int x, int y, const Population& source);
	void SetNewCellState(int x, int y, Population& temp, int newState);

	void EndSimulation();	

	int ReadPatternFromFile();
	int CheckIfPatternValid(const std::string& zeile, int zeilenCounter, int speicher, int& errorCounter);
	void EndIfError(int errorCounter);
	void SetCoordinatesFromFile();

private:
	CellularAutomaton();
	unsigned int generationCounter = 1;
	bool stepwiseSimulation = false;
	unsigned int waitingTimeInMilliseconds = 800;
	int simulationStatus = 0;
	int inputSimulationstyle = 0;
	const int drawPhase = 1;
	const int inputPhase = 0;
	const int deadLonelinessLimit = 2;
	const int surviveLimit = 3;
};
