#ifndef NewPopulationDialog_H
#define NewPopulationDialog_H

#include <QDialog>

namespace Ui {
class NewPopulationDialog;
}

class NewPopulationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewPopulationDialog(QWidget *parent = nullptr);
    ~NewPopulationDialog();
    Ui::NewPopulationDialog *ui;
    void SetMsg(const QString& msg);
   int GetWidth();
   int GetHeight();
   int GetTotalCells(int width, int height);
   void SetLabelText();
    //QString vordefinierterPfad;
   int width = 0;
   int height = 0;

private slots:
    void on_comboBoxWidth_clicked();
void on_comboBoxHeight_clicked();

private:

};

#endif // NewPopulationDialog_H
